/**
 * Created by nisha on 11-Nov-16.
 */
$(window).load(function() {
    var theWindow = $(window),
        $bg = $("#slider-container img"), aspectRatio = $bg.width() / $bg.height();
    function resizeBg() {
        if ( (theWindow.width() / theWindow.height()) < aspectRatio )
            $bg.removeClass().addClass('bgheight');
        else
            $bg.removeClass().addClass('bgwidth');
    }
    theWindow.resize(resizeBg).trigger("resize");
});